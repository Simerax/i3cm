# i3cm

A tool for managing/merging i3wm configs.

Barely usable so far. It is cabable of parsing and merging my personal config __but__ there are still a LOT of commands not implemented.
Its very likely that you will get a "Unexpected Token" error on your config

### Why?

Well for me its quite simple. I have a Desktop and a Laptop, both run i3.
About 80% of the config is the same and I want to have this part in some kind of "base config".
The other 20% are different (font size, spacing, etc.). These settings should be in separate config files.
Now I want to be able to generate a config for my specific target with the press of a button.

#### Why not just copy paste?

Sometimes I want to define a specific command in my base config but overwrite it in a more specific config.
To be able to do this the tool needs to understand the structure of i3wm configs. A simple copy and paste would have unintended Side effects.



## Usage

Configs are parsed left to right. Left being the most generic/basic configuration. The more you go to the right the more specific the configs get.
Every config to the right gets applied on the config to the left.

`i3cm <base_config> <more specific config> <even more specific config> <... etc>`

## Contributors

- [Simerax](https://gitlab.com/Simerax) - creator and maintainer
