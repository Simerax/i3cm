require "./spec_helper"

def run_parse(text : String)
  I3cm::Parser.new.parse(I3cm::Lexer.lex(text))
end

describe I3cm::Parser do
  it "parses set $mod Mod4" do
    tokens = I3cm::Lexer.lex("set $mod Mod4")
    ast = I3cm::Parser.new.parse(tokens)

    ast.size.should eq(1)

    ast[0].should be_a(I3cm::AST::Set)
    set = ast[0]
    if set.is_a?(I3cm::AST::Set)
      set.variable.should eq("$mod")
      value = set.value
      value.should be_a(I3cm::AST::Value)
      if value.is_a?(I3cm::AST::Value)
        value.value.should eq("Mod4")
      end
    end
  end

  it "parses bindsym r restart" do
    ast = I3cm::Parser.new.parse(I3cm::Lexer.lex("bindsym r restart"))
    ast.size.should eq(1)
    bindsym = ast[0]
    bindsym.should be_a(I3cm::AST::Bindsym)
    if bindsym.is_a?(I3cm::AST::Bindsym)
      bindsym.release_flag.should eq(false)

      key = bindsym.key
      key.should be_a(I3cm::AST::Key)

      key.modifier.size.should eq(0)

      keysym = key.keysym
      keysym.should be_a(I3cm::AST::Value)
      if keysym.is_a?(I3cm::AST::Value)
        keysym.value.should eq("r")
      end
    end
  end

  it "parses bindsym $mod +Shift+r restart" do
    tokens = I3cm::Lexer.lex("bindsym $mod +Shift+r restart")
    ast = I3cm::Parser.new.parse(tokens)

    ast.size.should eq(1)
    bindsym = ast[0]

    bindsym.should be_a(I3cm::AST::Bindsym)
    if bindsym.is_a?(I3cm::AST::Bindsym)
      bindsym.release_flag.should eq(false)

      key = bindsym.key.keysym
      key.should be_a(I3cm::AST::Value)
      if key.is_a?(I3cm::AST::Value)
        key.value.should eq("r")
      end
      bindsym.key.modifier.size.should eq(2)

      modifiers = bindsym.key.modifier

      dollar_mod = modifiers[0]
      dollar_mod.should be_a(I3cm::AST::Variable)
      if dollar_mod.is_a?(I3cm::AST::Variable)
        dollar_mod.name.should eq("$mod")
      end

      shift = modifiers[1]
      shift.should be_a(I3cm::AST::Value)
      if shift.is_a?(I3cm::AST::Value)
        shift.value.should eq("Shift")
      end

      commands = bindsym.commands
      commands.size.should eq(1)

      restart = commands[0].should be_a I3cm::AST::Custom
      restart.command.should eq "restart"
    end
  end

  it "parses bindsym $mod + Shift + r restart" do
    tokens = I3cm::Lexer.lex("bindsym $mod + Shift + r restart")
    ast = I3cm::Parser.new.parse(tokens)

    ast.size.should eq(1)
    bindsym = ast[0]

    bindsym.should be_a(I3cm::AST::Bindsym)
    if bindsym.is_a?(I3cm::AST::Bindsym)
      bindsym.release_flag.should eq(false)

      key = bindsym.key.keysym
      key.should be_a(I3cm::AST::Value)
      if key.is_a?(I3cm::AST::Value)
        key.value.should eq("r")
      end
      bindsym.key.modifier.size.should eq(2)

      modifiers = bindsym.key.modifier

      dollar_mod = modifiers[0]
      dollar_mod.should be_a(I3cm::AST::Variable)
      if dollar_mod.is_a?(I3cm::AST::Variable)
        dollar_mod.name.should eq("$mod")
      end

      shift = modifiers[1]
      shift.should be_a(I3cm::AST::Value)
      if shift.is_a?(I3cm::AST::Value)
        shift.value.should eq("Shift")
      end

      commands = bindsym.commands
      commands.size.should eq(1)

      restart = commands[0].should be_a I3cm::AST::Custom
      restart.command.should eq "restart"
    end
  end

  it "parses a mode statement" do
    text = "
      mode \"resize\" {


        bindsym Return mode \"default\"
      }
      bindsym $mod+r mode \"resize\"
    "
    tokens = I3cm::Lexer.lex(text)
    ast = I3cm::Parser.new.parse(tokens)

    ast.size.should eq(2)

    mode_config = ast[0]
    mode_config.should be_a(I3cm::AST::ModeConfig)

    if mode_config.is_a?(I3cm::AST::ModeConfig)
      mode_config.pango_markup.should eq(false)
      name = mode_config.name
      name.should be_a(I3cm::AST::Value)
      if name.is_a?(I3cm::AST::Value)
        name.value.should eq("\"resize\"")
      end

      mode_config.config.size.should eq(1)
      bindsym = mode_config.config[0]
      bindsym.should be_a(I3cm::AST::Bindsym)
      if bindsym.is_a?(I3cm::AST::Bindsym)
        bindsym.release_flag.should eq(false)
        key = bindsym.key.keysym
        key.should be_a(I3cm::AST::Value)
        if key.is_a?(I3cm::AST::Value)
          key.value.should eq("Return")
        end

        bindsym.key.modifier.size.should eq(0)
        bindsym.commands.size.should eq(1)
      end
    end

    bindsym = ast[1]
    bindsym.should be_a(I3cm::AST::Bindsym)
  end

  # TODO: Check if its actual possible to detect that "mode" is a invalid key. Maybe its allowed idk
  # it "raises on unexpected token" do
  #   text = "bindsym mode \"resize\""
  #   exception_thrown = false
  #   begin
  #     I3cm::Parser.new.parse(I3cm::Lexer.lex(text))
  #   rescue ex
  #     exception_thrown = true
  #     ex.message.should eq("Unexpected Token \"mode\" at Line: 1 Column: 8 File: none")
  #     ex.should be_a(I3cm::Parser::UnexpectedToken)
  #   end

  #   exception_thrown.should eq(true)
  # end

  it "parses a pango font" do
    ast = I3cm::Parser.new.parse(I3cm::Lexer.lex("font pango:$font $font_size"))
    ast.size.should eq(1)

    font = ast[0]
    font.should be_a(I3cm::AST::Font)

    if font.is_a?(I3cm::AST::Font)
      size = font.size
      size.should be_a(I3cm::AST::Variable)
      if size.is_a?(I3cm::AST::Variable)
        size.name.should eq("$font_size")
      end
      font.font.should eq("$font")
    end
  end

  it "parses an exec" do
    ast = I3cm::Parser.new.parse(I3cm::Lexer.lex("exec --no-startup-id mpc volume +5"))
    ast.size.should eq(1)

    exec = ast[0]

    exec.should be_a(I3cm::AST::Exec)
    if exec.is_a?(I3cm::AST::Exec)
      exec.always.should eq(false)
      exec.no_startup_id.should eq(true)
      exec.command.size.should eq(3)
    end
  end

  it "parses an exec_always" do
    ast = I3cm::Parser.new.parse(I3cm::Lexer.lex("exec_always --no-startup-id mpc volume +5"))
    ast.size.should eq(1)

    exec = ast[0]

    exec.should be_a(I3cm::AST::Exec)
    if exec.is_a?(I3cm::AST::Exec)
      exec.always.should eq(true)
      exec.no_startup_id.should eq(true)
      exec.command.size.should eq(3)
    end
  end

  it "parses a set with a value which has spaces" do
    ast = I3cm::Parser.new.parse(I3cm::Lexer.lex("set $mode_system System (s)hutdown"))
    ast.size.should eq(1)

    set = ast[0]

    set.should be_a(I3cm::AST::Set)
    if set.is_a?(I3cm::AST::Set)
      set.variable.should eq("$mode_system")
      val = set.value
      val.should be_a(I3cm::AST::Value)
      if val.is_a?(I3cm::AST::Value)
        val.value.should eq("System (s)hutdown")
      end
    end
  end

  it "parses a mode with multiple layers of nested toplevel statements" do
    text = "
      mode \"$mode_system\" {
        bindsym s exec --no-startup-id systemctl poweroff
        bindsym r exec --no-startup-id systemctl reboot


        bindsym l exec --no-startup-id loginctl terminate-user $USER

        bindsym Escape mode \"default\"
      }
    "
    ast = run_parse(text)
    ast.size.should eq(1)

    mode = ast[0]
    mode.should be_a(I3cm::AST::ModeConfig)
    if mode.is_a?(I3cm::AST::ModeConfig)
      mode.pango_markup.should eq(false)
      mode.config.size.should eq(4)
    end
  end

  it "parses floating_modifier $mod" do
    ast = run_parse("floating_modifier $mod")
    ast.size.should eq(1)

    fm = ast[0]
    fm.should be_a(I3cm::AST::FloatingModifier)
    if fm.is_a?(I3cm::AST::FloatingModifier)
      fm.modifier.should be_a(I3cm::AST::Variable)
    end
  end

  it "parses bindsym $mod +x [class=\"Firefox\"] kill" do
    ast = run_parse("bindsym $mod +x [class=\"Firefox\"] kill")

    ast.size.should eq(1)
    bindsym = ast[0]

    bindsym.should be_a(I3cm::AST::Bindsym)
    if bindsym.is_a?(I3cm::AST::Bindsym)
      bindsym.release_flag.should eq(false)

      k = bindsym.key
      keysym = k.keysym
      keysym.should be_a(I3cm::AST::Value)
      if keysym.is_a?(I3cm::AST::Value)
        keysym.value.should eq("x")
      end

      modifier = k.modifier
      modifier.size.should eq(1)
      dollar_mod = modifier[0]
      dollar_mod.should be_a(I3cm::AST::Variable)
      if dollar_mod.is_a?(I3cm::AST::Variable)
        dollar_mod.name.should eq("$mod")
      end

      criteria_list = bindsym.criteria_list
      criteria_list.should be_a(I3cm::AST::CriteriaList)
      if criteria_list.is_a?(I3cm::AST::CriteriaList)
        criteria_list.size.should eq(1)
        criteria_class_firefox = criteria_list[0]

        criteria_class_firefox.should be_a(I3cm::AST::Criteria)
        if criteria_class_firefox.is_a?(I3cm::AST::Criteria)
          key = criteria_class_firefox.key
          value = criteria_class_firefox.value

          key.should be_a(I3cm::AST::Value)
          if key.is_a?(I3cm::AST::Value)
            key.value.should eq("class")
          end

          value.should be_a(I3cm::AST::Value)
          if value.is_a?(I3cm::AST::Value)
            value.value.should eq("\"Firefox\"")
          end
        end
      end

      bindsym.commands.size.should eq(1)
    end
  end
  it "parses for_window [class=\"Mail\"] move to workspace $workspace2" do
    ast = run_parse("for_window [class=\"Mail\"] move to workspace $workspace2")
    ast.size.should eq(1)

    for_window = ast[0].should be_a I3cm::AST::ForWindow

    criteria = for_window.criteria.should be_a I3cm::AST::CriteriaList
    criteria.size.should eq 1

    class_mail = criteria[0].should be_a I3cm::AST::Criteria

    key = class_mail.key.should be_a I3cm::AST::Value
    key.value.should eq "class"

    value = class_mail.value.should be_a I3cm::AST::Value
    value.value.should eq "\"Mail\""

    command = for_window.commands[0].should be_a I3cm::AST::Move
    command.absolute.should be_false

    type = command.type.should be_a I3cm::AST::Move::Type
    type.should eq I3cm::AST::Move::Type::To

    where = command.where.should be_a I3cm::AST::Variable
    where.name.should eq "$workspace2"
  end

  it "parses bindsym $mod+Shift+j move left" do
    ast = run_parse("bindsym $mod+Shift+j move left")
    ast.size.should eq 1

    bindsym = ast[0].should be_a I3cm::AST::Bindsym
    move = bindsym.commands[0].should be_a I3cm::AST::Move
    move.absolute.should be_false
    move_type = move.type.should be_a I3cm::AST::Move::Type
    move_type.should eq I3cm::AST::Move::Type::Left
  end

  it "parses bindsym $mod+Shift+j move right" do
    ast = run_parse("bindsym $mod+Shift+j move right")
    ast.size.should eq 1

    bindsym = ast[0].should be_a I3cm::AST::Bindsym
    move = bindsym.commands[0].should be_a I3cm::AST::Move
    move.absolute.should be_false
    move_type = move.type.should be_a I3cm::AST::Move::Type
    move_type.should eq I3cm::AST::Move::Type::Right
  end

  it "parses bindsym $mod+Shift+j move down" do
    ast = run_parse("bindsym $mod+Shift+j move down")
    ast.size.should eq 1

    bindsym = ast[0].should be_a I3cm::AST::Bindsym
    move = bindsym.commands[0].should be_a I3cm::AST::Move
    move.absolute.should be_false
    move_type = move.type.should be_a I3cm::AST::Move::Type
    move_type.should eq I3cm::AST::Move::Type::Down
  end

  it "parses bindsym $mod+Shift+j move up" do
    ast = run_parse("bindsym $mod+Shift+j move up")
    ast.size.should eq 1

    bindsym = ast[0].should be_a I3cm::AST::Bindsym
    move = bindsym.commands[0].should be_a I3cm::AST::Move
    move.absolute.should be_false
    move_type = move.type.should be_a I3cm::AST::Move::Type
    move_type.should eq I3cm::AST::Move::Type::Up
  end

  it "parses bindsym $mod+Shift+j move up 10 px" do
    ast = run_parse("bindsym $mod+Shift+j move up 10 px")
    ast.size.should eq 1

    bindsym = ast[0].should be_a I3cm::AST::Bindsym
    move = bindsym.commands[0].should be_a I3cm::AST::Move
    move.absolute.should be_false
    move_type = move.type.should be_a I3cm::AST::Move::Type
    move_type.should eq I3cm::AST::Move::Type::Up

    where = move.where.should be_a I3cm::AST::Value
    where.value.should eq "10"

    unit = move.unit.should be_a I3cm::AST::Value
    unit.value.should eq "px"
  end

  it "parses bindsym $mod+Shift+j move left 32 ppt" do
    ast = run_parse("bindsym $mod+Shift+j move left 32 ppt")
    ast.size.should eq 1

    bindsym = ast[0].should be_a I3cm::AST::Bindsym
    move = bindsym.commands[0].should be_a I3cm::AST::Move
    move.absolute.should be_false
    move_type = move.type.should be_a I3cm::AST::Move::Type
    move_type.should eq I3cm::AST::Move::Type::Left

    where = move.where.should be_a I3cm::AST::Value
    where.value.should eq "32"

    unit = move.unit.should be_a I3cm::AST::Value
    unit.value.should eq "ppt"
  end

  it "parses bindsym $mod+Shift+j move down 3600" do
    ast = run_parse("bindsym $mod+Shift+j move down 3600")
    ast.size.should eq 1

    bindsym = ast[0].should be_a I3cm::AST::Bindsym
    move = bindsym.commands[0].should be_a I3cm::AST::Move
    move.absolute.should be_false
    move_type = move.type.should be_a I3cm::AST::Move::Type
    move_type.should eq I3cm::AST::Move::Type::Down

    where = move.where.should be_a I3cm::AST::Value
    where.value.should eq "3600"

    unit = move.unit.should be_a I3cm::AST::Value
    unit.value.should eq ""
  end

  it "parses bindsym $mod+Shift+1 move container to workspace $workspace1" do
    ast = run_parse("bindsym $mod+Shift+j move container to workspace $workspace1")
    ast.size.should eq 1

    bindsym = ast[0].should be_a I3cm::AST::Bindsym
    move = bindsym.commands[0].should be_a I3cm::AST::Move
    move.absolute.should be_false
    move_type = move.type.should be_a I3cm::AST::Move::Type
    move_type.should eq I3cm::AST::Move::Type::To

    where = move.where.should be_a I3cm::AST::Variable
    where.name.should eq "$workspace1"

    unit = move.unit.should be_a I3cm::AST::Value
    unit.value.should eq ""

    move.what.should eq ("container")
  end
  it "parses bindsym $mod+Shift+1 move window to workspace 4" do
    ast = run_parse("bindsym $mod+Shift+j move window to workspace 4")
    ast.size.should eq 1

    bindsym = ast[0].should be_a I3cm::AST::Bindsym
    move = bindsym.commands[0].should be_a I3cm::AST::Move
    move.absolute.should be_false
    move_type = move.type.should be_a I3cm::AST::Move::Type
    move_type.should eq I3cm::AST::Move::Type::To

    where = move.where.should be_a I3cm::AST::Value
    where.value.should eq "4"

    unit = move.unit.should be_a I3cm::AST::Value
    unit.value.should eq ""

    move.what.should eq ("window")
  end
  it "parses assign [class=\"Firefox\"] $workspace9" do
    ast = run_parse("assign [class=\"Firefox\"] $workspace")
    ast.size.should eq 1

    assign = ast[0].should be_a I3cm::AST::Assign
    where = assign.where.should be_a I3cm::AST::Variable
    where.name.should eq "$workspace"
    assign.where_prefix.should eq ""

    criterias = assign.criteria_list.should be_a I3cm::AST::CriteriaList
    criterias.size.should eq 1
  end

  it "parses assign [class=\"Firefox\"] number 2" do
    ast = run_parse("assign [class=\"Firefox\"] number 2")
    ast.size.should eq 1

    assign = ast[0].should be_a I3cm::AST::Assign
    where = assign.where.should be_a I3cm::AST::Value
    where.value.should eq "2"
    assign.where_prefix.should eq "number"

    criterias = assign.criteria_list.should be_a I3cm::AST::CriteriaList
    criterias.size.should eq 1
  end

  it "parses assign [class=\"Firefox\"] workspace $wk" do
    ast = run_parse("assign [class=\"Firefox\"] workspace $wk")
    ast.size.should eq 1

    assign = ast[0].should be_a I3cm::AST::Assign
    where = assign.where.should be_a I3cm::AST::Variable
    where.name.should eq "$wk"
    assign.where_prefix.should eq "workspace"

    criterias = assign.criteria_list.should be_a I3cm::AST::CriteriaList
    criterias.size.should eq 1
  end

  it "parses client colors" do
    text = "
    client.focused          #4c7899 #285577 #ffffff #2e9ef4   #285577
    client.focused_inactive #333333 #5f676a #ffffff #484e50   #5f676a
    client.unfocused        #333333 #222222 $my_var #292d2e   #222222
    # some random comment in the middle
    client.urgent           #2f343a #900000 #ffffff #900000   #900000
    client.placeholder      #000000 #0c0c0c #ffffff #000000   #0c0c0c

    client.background       #ffffff
    "
    ast = run_parse(text)
    ast.size.should eq 6

    focused = ast[0].should be_a I3cm::AST::Color
    border = focused.border.should be_a I3cm::AST::Value
    border.value.should eq "#4c7899"

    unfocused = ast[2].should be_a I3cm::AST::Color
    text = unfocused.text.should be_a I3cm::AST::Variable
    text.name.should eq "$my_var"

    urgent = ast[3].should be_a I3cm::AST::Color
    indicator = urgent.indicator.should be_a I3cm::AST::Value
    indicator.value.should eq "#900000"
  end
end
