require "./spec_helper"

describe I3cm::Lexer do
  it "lexes a set statement" do
    result = I3cm::Lexer.lex("set $mod Mod4")

    result[0].value.should eq("set")
    result[0].type.should eq(I3cm::Token::Type::SET)

    result[1].value.should eq("$mod")
    result[1].type.should eq(I3cm::Token::Type::VARIABLE)

    result[2].value.should eq("Mod4")
    result[2].type.should eq(I3cm::Token::Type::VALUE)
  end

  it "lexes a bindsym" do
    result = I3cm::Lexer.lex("bindsym $mod+w split h")
    result[0].value.should eq("bindsym")
    result[0].type.should eq(I3cm::Token::Type::BINDSYM)

    result[1].value.should eq("$mod+w")
    result[1].type.should eq(I3cm::Token::Type::VARIABLE)

    result[2].value.should eq("split")
    result[2].type.should eq(I3cm::Token::Type::VALUE)

    result[3].value.should eq("h")
    result[3].type.should eq(I3cm::Token::Type::VALUE)
  end

  it "lexes bindsym $mod+Shift+3 move container to workspace $workspace3" do
    result = I3cm::Lexer.lex("bindsym $mod+Shift+3 move container to workspace $workspace3")

    result[0].value.should eq("bindsym")
    result[0].type.should eq(I3cm::Token::Type::BINDSYM)

    result[1].value.should eq("$mod+Shift+3")
    result[1].type.should eq(I3cm::Token::Type::VARIABLE)

    result[2].value.should eq("move")
    result[2].type.should eq(I3cm::Token::Type::VALUE)

    result[3].value.should eq("container")
    result[3].type.should eq(I3cm::Token::Type::VALUE)

    result[4].value.should eq("to")
    result[4].type.should eq(I3cm::Token::Type::VALUE)

    result[5].value.should eq("workspace")
    result[5].type.should eq(I3cm::Token::Type::VALUE)

    result[6].value.should eq("$workspace3")
    result[6].type.should eq(I3cm::Token::Type::VARIABLE)
  end

  it "lexes an assign" do
    result = I3cm::Lexer.lex("assign [class=\"my cool name with space\"] $workspace9")

    result[0].value.should eq("assign")
    result[0].type.should eq(I3cm::Token::Type::ASSIGN)

    result[1].value.should eq("[")
    result[1].type.should eq(I3cm::Token::Type::LEFT_SQUARE_BRACKET)

    result[2].value.should eq("class")
    result[2].type.should eq(I3cm::Token::Type::VALUE)

    result[3].value.should eq("=")
    result[3].type.should eq(I3cm::Token::Type::OP_ASSIGN)

    result[4].value.should eq("\"my cool name with space\"")
    result[4].type.should eq(I3cm::Token::Type::STRING)

    result[5].value.should eq("]")
    result[5].type.should eq(I3cm::Token::Type::RIGHT_SQUARE_BRACKET)

    result[6].value.should eq("$workspace9")
    result[6].type.should eq(I3cm::Token::Type::VARIABLE)
  end

  it "lexes a mode" do
    text = "mode \"resize\" {
      bindsym Return mode \"default\"
    }"
    result = I3cm::Lexer.lex(text)

    result.size.should eq(11)

    result[0].value.should eq("mode")
    result[0].type.should eq(I3cm::Token::Type::MODE)

    result[1].value.should eq("\"resize\"")
    result[1].type.should eq(I3cm::Token::Type::STRING)

    result[2].value.should eq("{")
    result[2].type.should eq(I3cm::Token::Type::LEFT_BRACKET)

    result[3].type.should eq(I3cm::Token::Type::NEWLINE)

    result[4].value.should eq("bindsym")
    result[4].type.should eq(I3cm::Token::Type::BINDSYM)

    result[5].value.should eq("Return")
    result[5].type.should eq(I3cm::Token::Type::VALUE)

    result[6].value.should eq("mode")
    result[6].type.should eq(I3cm::Token::Type::MODE)

    result[7].value.should eq("\"default\"")
    result[7].type.should eq(I3cm::Token::Type::STRING)

    result[8].type.should eq(I3cm::Token::Type::NEWLINE)

    result[9].value.should eq("}")
    result[9].type.should eq(I3cm::Token::Type::RIGHT_BRACKET)

    result[10].type.should eq(I3cm::Token::Type::NEWLINE)
  end

  it "lexes a font" do
    result = I3cm::Lexer.lex("font pango:$font $font_size")

    result.size.should eq(6)

    result[0].value.should eq("font")
    result[0].type.should eq(I3cm::Token::Type::FONT)

    result[1].value.should eq("pango")
    result[1].type.should eq(I3cm::Token::Type::VALUE)

    result[2].value.should eq(":")
    result[2].type.should eq(I3cm::Token::Type::COLON)

    result[3].value.should eq("$font")
    result[3].type.should eq(I3cm::Token::Type::VARIABLE)

    result[4].value.should eq("$font_size")
    result[4].type.should eq(I3cm::Token::Type::VARIABLE)

    result[5].type.should eq(I3cm::Token::Type::NEWLINE)
  end

  it "lexes a exec" do
    result = I3cm::Lexer.lex("exec --no-startup-id mpc volume +5")
    result.size.should eq(6)

    result[0].value.should eq("exec")
    result[0].type.should eq(I3cm::Token::Type::EXEC)

    result[1].value.should eq("--no-startup-id")
    result[1].type.should eq(I3cm::Token::Type::VALUE)

    result[2].value.should eq("mpc")
    result[2].type.should eq(I3cm::Token::Type::VALUE)

    result[3].value.should eq("volume")
    result[3].type.should eq(I3cm::Token::Type::VALUE)

    result[4].value.should eq("+5")
    result[4].type.should eq(I3cm::Token::Type::VALUE)
  end

  it "lexes a floating_modifier $mod" do
    result = I3cm::Lexer.lex("floating_modifier $mod")
    result.size.should eq(3)

    result[0].value.should eq("floating_modifier")
    result[0].type.should eq(I3cm::Token::Type::FLOATING_MODIFIER)

    result[1].value.should eq("$mod")
    result[1].type.should eq(I3cm::Token::Type::VARIABLE)
  end

  it "lexes for_window [class=\"urxvt\"] border pixel 1" do
    result = I3cm::Lexer.lex("for_window [class=\"urxvt\"] border pixel 1")
    result.size.should eq(10)

    result[0].value.should eq("for_window")
    result[0].type.should eq(I3cm::Token::Type::FOR_WINDOW)

    result[1].value.should eq("[")
    result[1].type.should eq(I3cm::Token::Type::LEFT_SQUARE_BRACKET)

    result[2].value.should eq("class")
    result[2].type.should eq(I3cm::Token::Type::VALUE)

    # TODO: Maybe we should get rid of "=" as well.
    # It could probably pop up in someones command and cause a lot of trouble
    result[3].value.should eq("=")
    result[3].type.should eq(I3cm::Token::Type::OP_ASSIGN)

    result[4].value.should eq("\"urxvt\"")
    result[4].type.should eq(I3cm::Token::Type::STRING)

    result[5].value.should eq("]")
    result[5].type.should eq(I3cm::Token::Type::RIGHT_SQUARE_BRACKET)

    result[6].value.should eq("border")
    result[6].type.should eq(I3cm::Token::Type::VALUE)

    result[7].value.should eq("pixel")
    result[7].type.should eq(I3cm::Token::Type::VALUE)

    result[8].value.should eq("1")
    result[8].type.should eq(I3cm::Token::Type::VALUE)
  end
  it "allow splitting of commands by backslash" do
    input = "set $cmd \\
      \"my command\""

    result = I3cm::Lexer.lex(input)
    # result.size.should eq(4)

    result[0].value.should eq("set")
    result[0].type.should eq(I3cm::Token::Type::SET)

    result[1].value.should eq("$cmd")
    result[1].type.should eq(I3cm::Token::Type::VARIABLE)

    result[2].value.should eq("\"my command\"")
    result[2].type.should eq(I3cm::Token::Type::STRING)
  end

  it "lexes chained commands" do
    result = I3cm::Lexer.lex("exec $cmd; mode \"default\"")

    result[0].value.should eq("exec")
    result[0].type.should eq(I3cm::Token::Type::EXEC)

    result[1].value.should eq("$cmd")
    result[1].type.should eq(I3cm::Token::Type::VARIABLE)

    result[2].value.should eq(";")
    result[2].type.should eq(I3cm::Token::Type::SEMICOLON)

    result[3].value.should eq("mode")
    result[3].type.should eq(I3cm::Token::Type::MODE)

    result[4].value.should eq("\"default\"")
    result[4].type.should eq(I3cm::Token::Type::STRING)
  end

  it "lexes a client.focused" do
    result = I3cm::Lexer.lex("client.focused          #4c7899 #285577 #ffffff #2e9ef4   #285577")
    result.size.should eq 7
    result.[0].value.should eq "client.focused"
    result.[4].value.should eq "#2e9ef4"
  end
end
