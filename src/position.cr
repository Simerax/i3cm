module I3cm
  class Position
    property line : Int32
    property column : Int32
    property file : String

    def initialize(@line = -1, @column = -1, @file = "none")
    end

    def to_s
      "Line: #{line} Column: #{column} File: #{file}"
    end
  end
end
