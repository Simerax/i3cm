module I3cm
  class Checks
    struct Result
      enum Type
        Warning
        Error
      end
      property type : Type
      property msg : String

      def initialize(@msg, @type)
      end
    end

    def self.run(config : AST::Node)
      UndefinedVars.new.run(config) do |x|
        yield x
      end
      DuplicateKeybindings.new.run(config) do |x|
        yield x
      end
    end

    class DuplicateKeybindings < Visitor
      def initialize
        @bindsym = [] of AST::Bindsym
        @unique = [] of AST::Bindsym

        # for now we ignore bindings inside mode configs
        @inside_mode_config = false
      end

      def run(config : AST::Node)
        reset()
        visit(config)
        output_errors do |x|
          yield x
        end
      end

      def output_errors
        @unique = [] of AST::Bindsym

        @bindsym.each do |b|
          if @unique.find { |bu| bu.key == b.key }
            key_str = ""

            b.key.modifier.each do |m|
              if m.is_a?(AST::Variable)
                key_str += m.name + "+"
              elsif m.is_a?(AST::Value)
                key_str += m.value + "+"
              end
            end
            keysym = b.key.keysym
            if keysym.is_a?(AST::Variable)
              key_str += keysym.name
            elsif keysym.is_a?(AST::Value)
              key_str += keysym.value
            end

            yield Result.new("Key \"#{key_str}\" is not unique!", Result::Type::Error)
          else
            @unique << b
          end
        end
      end

      def reset
        @bindsym = [] of AST::Bindsym
        @unique = [] of AST::Bindsym
      end

      def visit(n : AST::ModeConfig)
        @inside_mode_config = true
        n.name.accept(self)
        n.config.each do |c|
          c.accept(self)
        end
        @inside_mode_config = false
      end

      def visit(n : AST::Bindsym)
        @bindsym << n unless @inside_mode_config
      end
    end

    class UndefinedVars < Visitor
      @set_vars : Array(AST::Set)
      @vars : Array(AST::Variable)

      def initialize
        @set_vars = [] of AST::Set
        @vars = [] of AST::Variable
      end

      def reset
        @set_vars = [] of AST::Set
        @vars = [] of AST::Variable
      end

      def run(config : AST::Node)
        reset()
        visit(config)
        output_warnings do |x|
          yield x
        end
      end

      private def output_warnings
        @vars.each do |v|
          defined = false
          @set_vars.each do |sv|
            defined = true if sv.variable == v.name
          end
          unless defined
            yield Result.new("Variable #{v.name} has never been set!", Result::Type::Warning)
          end
        end
      end

      def visit(n : AST::Set)
        @set_vars << n
      end

      def visit(n : AST::Variable)
        @vars << n
      end
    end
  end
end
