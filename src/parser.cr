require "./ast.cr"

module I3cm
  class Parser
    class UnexpectedToken < Exception
      property token : Token

      def initialize(@token : Token, extra = "")
        extra = " : " + extra if extra
        @message = "Unexpected Token \"#{@token.value}\" at #{@token.position.to_s}#{extra}"
      end
    end

    @index = 0
    @tokens = [] of Token

    def parse(tokens : Array(I3cm::Token))
      @tokens = tokens
      @index = 0

      commands = [] of AST::Command

      while @index < @tokens.size
        unless accept(Token::Type::NEWLINE) # just skip empty lines
          commands << parse_command(false)
        end
      end
      commands
    end

    private def parse_toplevel
    end

    private def parse_for_window
      criteria_list = parse_criteria_block()
      command = parse_commands()
      AST::ForWindow.new(criteria_list, command)
    end

    private def parse_commands
      commands = [] of AST::Command
      until newline()
        commands << parse_command()
      end
      commands
    end

    private def parse_command(allow_custom = true)
      if accept("set")
        parse_set()
      elsif accept("floating_modifier")
        parse_floating_modifier()
      elsif accept("for_window")
        parse_for_window()
      elsif accept("bindsym")
        parse_bindsym()
      elsif accept("mode")
        parse_mode()
      elsif accept("exec")
        parse_exec()
      elsif accept("exec_always")
        parse_exec(true)
      elsif accept("font")
        parse_font()
      elsif accept("move")
        parse_move()
      elsif accept("assign")
        parse_assign()
      elsif accept("client.focused")
        parse_color(AST::Color::Class::Focused)
      elsif accept("client.focused_inactive")
        parse_color(AST::Color::Class::Focused_inactive)
      elsif accept("client.unfocused")
        parse_color(AST::Color::Class::Unfocused)
      elsif accept("client.urgent")
        parse_color(AST::Color::Class::Urgent)
      elsif accept("client.placeholder")
        parse_color(AST::Color::Class::Placeholder)
      elsif accept("client.background")
        parse_color(AST::Color::Class::Background)
      elsif allow_custom
        parse_custom_command()
      else
        raise UnexpectedToken.new(next_tok(), "This command is either invalid or not yet implemented. In the case of the latter open an issue on gitlab!")
      end
    end

    private def parse_color(color_class)
      border = AST::Value.new("")
      background = AST::Value.new("")
      text = AST::Value.new("")
      indicator = AST::Value.new("")
      child_border = AST::Value.new("")

      border = make_var_or_val_node(next_tok()) unless end_of_command?
      background = make_var_or_val_node(next_tok()) unless end_of_command?
      text = make_var_or_val_node(next_tok()) unless end_of_command?
      indicator = make_var_or_val_node(next_tok()) unless end_of_command?
      child_border = make_var_or_val_node(next_tok()) unless end_of_command?

      AST::Color.new(color_class, border, background, text, indicator, child_border)
    end

    private def parse_assign
      criteria_list = parse_criteria_block
      where_prefix = ""
      if accept("number")
        where_prefix = "number"
      elsif accept("workspace")
        where_prefix = "workspace"
      end
      where = make_var_or_val_node(next_tok())
      AST::Assign.new(criteria_list, where, where_prefix)
    end

    private def parse_custom_command
      cmd = ""
      until end_of_command?
        cmd += next_tok().value + " "
      end
      cmd = cmd.chomp(" ")
      AST::Custom.new(cmd)
    end

    private def parse_move
      absolute = false
      if accept("absolute")
        absolute = true
      end

      what = ""

      if peek("window") || peek("container")
        what = next_tok().value
      end

      if accept("to")
        # TODO: is there ever anything else then "to workspace"?
        expect("workspace")
        where = make_var_or_val_node(next_tok())
        unless end_of_command?
          raise UnexpectedToken.new(next_tok())
        end
        AST::Move.new(AST::Move::Type::To, where, absolute, AST::Value.new(""), what)
      elsif peek("left") || peek("right") || peek("down") || peek("up")
        where = next_tok()
        type = AST::Move::Type::UNKNOWN
        type = AST::Move::Type::Left if where.value == "left"
        type = AST::Move::Type::Right if where.value == "right"
        type = AST::Move::Type::Down if where.value == "down"
        type = AST::Move::Type::Up if where.value == "up"

        move = AST::Move.new(type)
        move.absolute = absolute
        move.what = what

        if end_of_command?
          move
        else
          where = make_var_or_val_node(next_tok())
          move.where = where if where.is_a?(AST::Value)
          move.where = where if where.is_a?(AST::Variable)
          if peek("px") || peek("ppt")
            unit = make_var_or_val_node(next_tok())
            move.unit = unit if unit.is_a?(AST::Value)
            move.unit = unit if unit.is_a?(AST::Variable)
          end
        end
        move
      else
        raise UnexpectedToken.new(next_tok(), "This subtype of the move command has not yet been implemented")
      end
    end

    private def parse_floating_modifier
      fm = AST::FloatingModifier.new(make_var_or_val_node(next_tok))
      unless end_of_command?()
        raise UnexpectedToken.new(next_tok())
      end
      fm
    end

    private def parse_exec(always = false)
      exec = AST::Exec.new
      exec.always = always
      if accept("--no-startup-id")
        exec.no_startup_id = true
      end

      command = [] of AST::Node
      until end_of_command?()
        command << make_var_or_val_node(next_tok())
      end
      exec.command = command
      exec
    end

    private def parse_font
      font = AST::Font.new
      if accept("pango")
        font.pango = true
        expect(":")
      end

      options = [] of Token
      until end_of_command?
        options << next_tok()
      end

      size = options.pop
      if size.type == Token::Type::VALUE
        font.size = AST::Value.new(size.value)
      elsif size.type == Token::Type::VARIABLE
        font.size = AST::Variable.new(size.value)
      else
        raise UnexpectedToken.new(size)
      end

      font_family = [] of String
      options.each do |o|
        font_family << o.value
      end

      font.font = font_family.join(" ")
      font
    end

    private def parse_mode
      pango_markup = false
      if accept("--pango_markup")
        pango_markup = true
      end

      name_tok = next_tok()
      name = make_var_or_val_node(name_tok)

      if accept("{") # Mode config
        accept(Token::Type::NEWLINE)
        mode = AST::ModeConfig.new(name, pango_markup)
        config = [] of AST::Command

        until accept("}")
          unless accept(Token::Type::NEWLINE)
            config << parse_command()
          end
        end
        mode.config = config
        mode
      else # Mode command
        AST::ModeCommand.new(name)
      end
    end

    private def make_var_or_val_node(t : Token)
      if t.type == Token::Type::VARIABLE
        AST::Variable.new(t.value)
      elsif t.type == Token::Type::VALUE || t.type == Token::Type::STRING
        AST::Value.new(t.value)
      else
        raise UnexpectedToken.new(t)
      end
    end

    private def parse_bindsym
      bindsym = AST::Bindsym.new
      if accept("--release")
        bindsym.release_flag = true
      end

      bindsym.key = parse_key()

      if peek("[")
        bindsym.criteria_list = parse_criteria_block()
      end

      bindsym.commands = parse_commands()
      bindsym
    end

    private def parse_criteria_block
      criteria_list = [] of AST::Criteria
      expect("[")
      until accept("]")
        criteria_list << parse_criteria()
      end
      AST::CriteriaList.new(criteria_list)
    end

    private def parse_criteria
      key = make_var_or_val_node(next_tok)
      expect("=")
      value = make_var_or_val_node(next_tok)
      accept(",")
      AST::Criteria.new(key, value)
    end

    private def parse_key
      keyparts = [] of AST::Node
      key = next_tok()

      # key could either be a single word or be split into multiple parts by space
      # $mod+Shift+r
      # $mod + Shift + r
      # $mod+Shift +r
      # all of these are valid

      if key.value.includes?('+')
        key.value.split('+').each do |k|
          if k.starts_with?("$")
            keyparts << AST::Variable.new(k)
          else
            keyparts << AST::Value.new(k)
          end
        end
      else
        if key.type == Token::Type::VARIABLE
          keyparts << AST::Variable.new(key.value)
        else
          keyparts << AST::Value.new(key.value)
        end
      end

      while accept("+") || peek().value.starts_with?("+")
        key = next_tok()
        if key.value.includes?('+')
          key.value.split('+').each do |k|
            if k.starts_with?("$")
              keyparts << AST::Variable.new(k)
            elsif k != ""
              keyparts << AST::Value.new(k)
            end
          end
        else
          if key.type == Token::Type::VARIABLE
            keyparts << AST::Variable.new(key.value)
          elsif key != ""
            keyparts << AST::Value.new(key.value)
          end
        end
      end

      AST::Key.new(keyparts.pop, keyparts)
    end

    private def parse_set
      variable = take(Token::Type::VARIABLE)
      value = [] of String
      until end_of_command?()
        value << next_tok().value
      end
      set = AST::Set.new(variable.value, AST::Value.new(value.join(" ")))
      set
    end

    private def next_tok
      tok = @tokens[@index]
      @index += 1
      tok
    end

    private def take(type : Token::Type)
      tok = next_tok()
      if tok.type == type
        tok
      else
        raise UnexpectedToken.new(tok)
      end
    end

    private def peek(type : Token::Type)
      if @tokens.size > @index
        @tokens[@index].type == type
      else
        false
      end
    end

    private def peek(w : String)
      if @tokens.size > @index
        @tokens[@index].value == w
      else
        false
      end
    end

    private def peek
      @tokens[@index]
    end

    private def accept(type : Token::Type)
      if @tokens.size > @index
        if @tokens[@index].type == type
          @index += 1
          true
        else
          false
        end
      else
        false
      end
    end

    private def accept(word : String)
      if @tokens.size > @index
        if @tokens[@index].value == word
          @index += 1
          true
        else
          false
        end
      else
        false
      end
    end

    private def expect(t : Token::Type)
      if @tokens[@index].type == t
        @index += 1
        true
      else
        raise UnexpectedToken.new(@tokens[@index])
      end
    end

    private def expect(word : String)
      if @tokens[@index].value == word
        @index += 1
        true
      else
        raise UnexpectedToken.new(@tokens[@index], "expected \"#{word}\"")
      end
    end

    private def newline
      accept(Token::Type::NEWLINE)
    end

    private def end_of_command?
      if accept(";")
        true
      else
        peek(Token::Type::NEWLINE)
      end
    end
  end
end
