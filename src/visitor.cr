module I3cm
  class Visitor
    def visit(n : AST::Root)
      n.childs.each do |c|
        c.accept(self)
      end
    end

    def visit(n : AST::Variable)
    end

    def visit(n : AST::Command)
    end

    def visit(n : AST::Value)
    end

    def visit(n : AST::Color)
      n.border.accept(self)
      n.background.accept(self)
      n.text.accept(self)
      n.indicator.accept(self)
      n.child_border.accept(self)
    end

    def visit(n : AST::Exec)
      n.command.each do |c|
        c.accept(self)
      end
    end

    def visit(v : AST::ForWindow)
      v.criteria.accept(self)
      v.commands.each do |c|
        c.accept(self)
      end
    end

    def visit(v : AST::CriteriaList)
      v.each do |c|
        c.accept(self)
      end
    end

    def visit(v : AST::Criteria)
      v.key.accept(self)
      v.value.accept(self)
    end

    def visit(v : AST::Move)
      type = v.type
      unless type.is_a?(AST::Move::Type)
        type.accept(self)
      end
      v.where.accept(self)
    end

    def visit(n : AST::Font)
      n.size.accept(self)
    end

    def visit(n : AST::FloatingModifier)
      n.modifier.accept(self)
    end

    def visit(n : AST::Bindsym)
      n.key.accept(self)
      n.commands.each do |c|
        c.accept(self)
      end
    end

    def visit(n : AST::Key)
      n.keysym.accept(self)
      n.modifier.each do |m|
        m.accept(self)
      end
    end

    def visit(n : AST::Set)
      n.value.accept(self)
    end

    def visit(n : AST::ModeCommand)
      n.name.accept(self)
    end

    def visit(n : AST::ModeConfig)
      n.name.accept(self)
      n.config.each do |c|
        c.accept(self)
      end
    end
  end

  class Printer < Visitor
    getter result : String

    def initialize
      @result = ""
    end

    def reset
      @result = ""
    end

    def visit(v : AST::Command)
      raise "raw AST::Command visited. This should probably never happen.."
    end

    def visit(n : AST::Assign)
      @result += "assign "
      n.criteria_list.accept(self)
      @result += "#{n.where_prefix} " if n.where_prefix
      n.where.accept(self)
      @result += "\n"
    end

    def visit(n : AST::Color)
      @result += "client.#{n.color_class.to_s.downcase} "
      n.border.accept(self)
      n.background.accept(self)
      n.text.accept(self)
      n.indicator.accept(self)
      n.child_border.accept(self)
      @result += "\n"
    end

    def visit(v : AST::Custom)
      @result += "#{v.command}\n"
    end

    def visit(v : AST::ForWindow)
      @result += "for_window "
      v.criteria.accept(self)
      v.commands.each do |c|
        c.accept(self)
        @result = @result.chomp
      end
      @result += "\n"
    end

    def visit(v : AST::CriteriaList)
      @result += "[ "
      v.each do |c|
        c.accept(self)
        @result = @result.chomp
      end
      @result += "] "
    end

    def visit(v : AST::Criteria)
      v.key.accept(self)

      # Its not necessary to remove the trailing space before the "=" but
      # it does look nicer in the output
      @result = @result.chomp(" ")
      @result += "="
      v.value.accept(self)
    end

    def visit(v : AST::Move)
      @result += "move "

      @result += "absolute " if v.absolute
      @result += "#{v.what} " if v.what

      type = v.type
      if type.is_a?(AST::Move::Type)
        @result += "#{type.to_s.downcase} "
        @result += "workspace " if type == AST::Move::Type::To
      else # Variable
        type.accept(self)
      end
      v.where.accept(self)
      @result += "\n"
    end

    def visit(n : AST::Value)
      @result += "#{n.value} "
    end

    def visit(n : AST::Variable)
      @result += "#{n.name} "
    end

    def visit(n : AST::Font)
      @result += "font "
      @result += "pango:" if n.pango
      @result += "#{n.font} "
      n.size.accept(self)
      @result += "\n"
    end

    def visit(n : AST::Exec)
      if n.always
        @result += "exec_always "
      else
        @result += "exec "
      end
      @result += "--no-startup-id " if n.no_startup_id
      n.command.each do |c|
        c.accept(self)
        @result = @result.chomp
      end
      @result += "\n"
    end

    def visit(n : AST::FloatingModifier)
      @result += "floating_modifier "
      n.modifier.accept(self)
      @result += "\n"
    end

    def visit(n : AST::Bindsym)
      @result += "bindsym "
      if n.release_flag
        @result += "--release "
      end

      n.key.accept(self)

      n.commands.each do |c|
        c.accept(self)
        @result = @result.chomp
        @result += "; "
      end
      @result = @result.chomp("; ")
      @result += "\n"
    end

    def visit(n : AST::Key)
      key = [] of String

      n.modifier.each do |m|
        if m.is_a?(AST::Value)
          key << m.value
        elsif m.is_a?(AST::Variable)
          key << m.name
        end
      end

      keysym = n.keysym
      if keysym.is_a?(AST::Variable)
        key << keysym.name
      elsif keysym.is_a?(AST::Value)
        key << keysym.value
      end

      @result += "#{key.join("+")} "
    end

    def visit(n : AST::Set)
      @result += "set #{n.variable} "
      n.value.accept(self)
      @result += "\n"
    end

    def visit(n : AST::ModeCommand)
      @result += "mode "
      n.name.accept(self)
      @result += "\n"
    end

    def visit(n : AST::ModeConfig)
      @result += "mode "
      if n.pango_markup
        @result += "--pango_markup "
      end
      n.name.accept(self)
      @result += "{\n"
      n.config.each do |c|
        c.accept(self)
      end
      @result += "}\n"
    end
  end
end
