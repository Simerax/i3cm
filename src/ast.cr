module I3cm::AST
  abstract class Node
    abstract def accept(v : Visitor)
  end

  class Root < Node
    property childs : Array(Command)

    def initialize(@childs)
    end

    def accept(v : Visitor)
      v.visit(self)
    end
  end

  class Command < Node
    def accept(v : Visitor)
      v.visit(self)
    end
  end

  class Color < Command
    enum Class
      Focused
      Focused_inactive
      Unfocused
      Urgent
      Placeholder
      Background
    end
    property color_class : Class
    property border : Variable | Value
    property background : Variable | Value
    property text : Variable | Value
    property indicator : Variable | Value
    property child_border : Variable | Value

    def initialize(@color_class, @border, @background, @text, @indicator, @child_border)
    end

    def accept(v : Visitor)
      v.visit(self)
    end
  end

  class Assign < Command
    property criteria_list : CriteriaList
    property where : Value | Variable
    property where_prefix : String

    def initialize(@criteria_list, @where, @where_prefix)
    end

    def ==(other : self) : Bool
      @criteria_list == other.criteria_list && @where == other.where && @where_prefix == other.where_prefix
    end

    def accept(v : Visitor)
      v.visit(self)
    end
  end

  class Custom < Command
    property command : String

    def initialize(@command = "")
    end

    def ==(other : self) : Bool
      @command == other.command
    end
  end

  class Move < Command
    enum Type
      Up
      Down
      Left
      Right
      Position
      To
      UNKNOWN
    end
    property where : Value | Variable
    property unit : Value | Variable
    property absolute : Bool
    property type : Type | Variable
    property what : String # container | window

    def initialize(@type)
      @where = AST::Value.new("")
      @unit = AST::Value.new("")
      @absolute = false
      @what = ""
    end

    def initialize(@type, @where, @absolute = false, @unit = AST::Value.new(""), @what = "")
    end

    def accept(v : Visitor)
      v.visit(self)
    end

    def ==(other : self)
      @where == other.where && @unit == other.unit && @absolute == other.absolute && @type == other.type && @what == other.what
    end
  end

  class ForWindow < Command
    property criteria : CriteriaList
    property commands : Array(Command)

    def initialize(@criteria, @commands)
    end

    def accept(v : Visitor)
      v.visit(self)
    end

    def ==(other : self)
      @criteria == other.criteria && @commands == other.commands
    end
  end

  class Criteria < Node
    property key : Value | Variable
    property value : Value | Variable

    def initialize(@key = Value.new(""), @value = Value.new(""))
    end

    def accept(v : Visitor)
      v.visit(self)
    end

    def ==(other : self)
      @key == other.key && @value == other.value
    end
  end

  class CriteriaList < Node
    include Enumerable(Criteria)
    property criteria : Array(Criteria)

    def initialize(@criteria = [] of Criteria)
    end

    def each
      @criteria.each do |c|
        yield c
      end
    end

    def [](index)
      @criteria[index]
    end

    def accept(v : Visitor)
      v.visit(self)
    end

    def ==(other : self)
      @criteria == other.criteria
    end
  end

  class FloatingModifier < Command
    property modifier : Value | Variable

    def initialize(@modifier)
    end

    def accept(v : Visitor)
      v.visit(self)
    end

    def ==(other : self)
      @modifier == other.modifier
    end
  end

  class Exec < Command
    property always : Bool # is it a exec or exec_always?
    property no_startup_id : Bool
    property command : Array(Node)

    def initialize(@command = [] of Node, @no_startup_id = false, @always = false)
    end

    def accept(v : Visitor)
      v.visit(self)
    end

    def ==(other : self)
      @always == other.always && @no_startup_id == other.no_startup_id && @command == other.command
    end
  end

  class Font < Command
    property pango : Bool
    property size : Variable | Value
    property font : String # FIXME: should be split into font and style options

    def initialize(@pango = false, @font = "", @size = AST::Value.new(""))
    end

    def accept(v : Visitor)
      v.visit(self)
    end

    def ==(other : self)
      @pango == other.pango && @size == other.size && @font == other.font
    end
  end

  class Value < Node
    getter value : String

    def initialize(@value : String)
    end

    def accept(v : Visitor)
      v.visit(self)
    end

    def ==(other : self)
      @value == other.value
    end
  end

  class Variable < Node
    getter name : String

    def initialize(@name : String)
    end

    def accept(v : Visitor)
      v.visit(self)
    end

    def ==(other : self)
      @name == other.name
    end
  end

  class Bindsym < Command
    property release_flag : Bool
    property key : Key
    property commands : Array(Command)
    property criteria_list : CriteriaList

    def initialize(@key = Key.new, @commands = [] of Command, @release_flag = false, @criteria_list = CriteriaList.new)
    end

    def accept(v : Visitor)
      v.visit(self)
    end

    def ==(other : self)
      @release_flag == other.release_flag && @key == other.key && @commands == other.commands && @criteria_list == other.criteria_list
    end
  end

  class Key < Node
    property keysym : Variable | Value
    property modifier : Array(Variable | Value)

    def initialize(@keysym = Value.new(""), @modifier = [] of Node)
    end

    def accept(v : Visitor)
      v.visit(self)
    end

    def ==(other : self)
      @keysym == other.keysym && @modifier == other.modifier
    end
  end

  class Set < Command
    property variable : String
    property value : Node

    def initialize(@variable : String, @value : Node)
    end

    def accept(v : Visitor)
      v.visit(self)
    end

    def ==(other : self)
      @variable == other.variable && @value == other.value
    end
  end

  class ModeCommand < Command
    property name : Variable | Value

    def initialize(@name = "")
    end

    def accept(v : Visitor)
      v.visit(self)
    end

    def ==(other : self)
      @name == other.name
    end
  end

  class ModeConfig < Command
    property pango_markup : Bool
    property name : Variable | Value
    property config : Array(Command)

    def initialize(@name = "", @pango_markup = false, @config = [] of Command)
    end

    def accept(v : Visitor)
      v.visit(self)
    end

    def ==(other : self)
      @pango_markup == other.pango_markup && @name == other.name && @config == other.config
    end
  end
end
