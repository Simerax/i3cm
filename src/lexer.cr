require "./position.cr"

module I3cm
  class Token
    enum Type
      SET
      FLOATING_MODIFIER
      VARIABLE
      ASSIGN
      BINDSYM
      MODE
      FOR_WINDOW
      VALUE
      FONT
      EXEC
      COLON
      SEMICOLON
      STRING
      NEWLINE
      OP_ASSIGN
      LEFT_BRACKET
      RIGHT_BRACKET
      LEFT_SQUARE_BRACKET
      RIGHT_SQUARE_BRACKET
      ClientFocused
      ClientFocusedInactive
      ClientUnfocused
      ClientUrgent
      ClientPlaceholder
      ClientBackground
    end

    getter value : String
    getter type : Type
    property position : Position

    def initialize(@value : String, @type : Type, @position = Position.new)
    end

    def self.getType(v : String)
      if v == "set"
        Type::SET
      elsif v == "font"
        Type::FONT
      elsif v == "for_window"
        Type::FOR_WINDOW
      elsif v == "bindsym"
        Type::BINDSYM
      elsif v == "mode"
        Type::MODE
      elsif v == "floating_modifier"
        Type::FLOATING_MODIFIER
      elsif v == "exec"
        Type::EXEC
      elsif v == "client.focused"
        Type::ClientFocused
      elsif v == "client.focused_inactive"
        Type::ClientFocusedInactive
      elsif v == "client.unfocused"
        Type::ClientUnfocused
      elsif v == "client.urgent"
        Type::ClientUrgent
      elsif v == "client.placeholder"
        Type::ClientPlaceholder
      elsif v == "client.background"
        Type::ClientBackground
      elsif v == "assign"
        Type::ASSIGN
      elsif v == "["
        Type::LEFT_SQUARE_BRACKET
      elsif v == "]"
        Type::RIGHT_SQUARE_BRACKET
      elsif v == "{"
        Type::LEFT_BRACKET
      elsif v == "}"
        Type::RIGHT_BRACKET
      elsif v == ":"
        Type::COLON
      elsif v == ";"
        Type::SEMICOLON
      elsif v == "="
        Type::OP_ASSIGN
      elsif v == "\n"
        Type::NEWLINE
      elsif /^\$/.match(v)
        Type::VARIABLE
      elsif v.starts_with?("\"") && v.ends_with?("\"")
        Type::STRING
      else
        Type::VALUE
      end
    end
  end

  class Lexer
    def self.lex(input : String, file = "none")
      i = 0
      chars = [] of Char
      input.each_char do |c|
        chars << c
      end
      chars << '\n'

      inside_comment = false

      tokens = [] of Token
      inside_string_literal = false

      line = 1
      column = 0
      word_begin_column = 0
      skip_until = -1

      # it is true that it is only allowed to have comments at the beginning of a line but you still can have space before the comment
      beginning_of_line = true

      word = ""
      while i < chars.size
        cc = chars[i] # Current Char

        if cc == '#' && beginning_of_line
          inside_comment = true
        end

        if !is_space(cc)
          beginning_of_line = false
        end

        # We treat a backslash followed by a newline as if there was no newline
        if cc == '\\'
          if chars.size > i + 1
            if chars[i + 1] == '\n'
              skip_until = i + 2
            end
          end
        end

        if i > skip_until
          unless inside_comment
            if word == ""
              word_begin_column = column
            end

            if cc == '"'
              inside_string_literal = inside_string_literal ? false : true
            end

            if inside_string_literal
              word += cc
            else
              if seperator?(cc) || operator?(cc)
                if word != ""
                  type = Token.getType(word)
                  tokens << Token.new(word, type, Position.new(line, word_begin_column, file))
                  word = ""
                end

                if operator?(cc)
                  tokens << Token.new("" + cc, Token.getType("" + cc), Position.new(line, word_begin_column, file))
                elsif cc == '\n'
                  tokens << Token.new("Newline", Token.getType("" + cc), Position.new(line, word_begin_column, file))
                end
              else
                word += cc
              end
            end
          end
        end

        i += 1
        column += 1
        if cc == '\n'
          inside_comment = false
          beginning_of_line = true
          line += 1
          column = 0
        end
      end

      tokens
    end

    def self.is_color(type : Token::Type)
      [Token::Type::ClientFocused, Token::Type::ClientFocusedInactive, Token::Type::ClientUnfocused, Token::Type::ClientUrgent, Token::Type::ClientPlaceholder, Token::Type::ClientBackground].any? { |t| t == type }
    end

    def self.seperator?(c : Char)
      [' ', '\n', '\t'].any? { |sep| sep == c }
    end

    def self.is_space(c : Char)
      [' ', '\t'].any? { |s| s == c }
    end

    def self.operator?(c : Char)
      ['=', '[', ']', '{', '}', ':', ';'].any? { |op| op == c }
    end
  end
end
