module I3cm
  class Merger
    def self.merge(base : AST::Root, change : AST::Root)
      merge_set(base, change)
      change.childs.each do |c|
        base.childs << c
      end
      base.childs = base.childs.uniq
      base
    end

    private def self.merge_set(base, change)
      base_set = SetFinder.new.find(base)
      change_set = SetFinder.new.find(change)

      to_be_added = [] of AST::Set

      change_set.each do |c|
        base_set.each do |b|
          if c.variable == b.variable
            if c.value != b.value
              b.value = c.value
            end
          end
        end
      end
    end
  end

  class SetFinder < Visitor
    @set_variables : Array(AST::Set)

    def initialize
      @set_variables = [] of AST::Set
    end

    def find(entry : AST::Node)
      visit(entry)
      @set_variables
    end

    def visit(n : AST::Set)
      @set_variables << n
    end
  end
end
